#! /usr/bin/make -f

VERSION=$(shell awk -F= '/config{version}[[:space:]]*=/ {print $$2}' src/cgi-bin/qemu-web-desktop.pl | sed -e 's/;.*//' -e 's/"//g' -e 's/[[:space:]]*//g' | tail -1 )

dist:
	tar --create \
		--auto-compress \
		--file=../qemu-web-desktop-$(VERSION).tar.gz \
		--transform=s,^./,qemu-web-desktop-$(VERSION)/, \
		--exclude=./src/html/desktop/machines \
		--exclude=./.git* \
		--exclude=*~ \
		--exclude=./debian \
		.
deb:
	debuild -b
